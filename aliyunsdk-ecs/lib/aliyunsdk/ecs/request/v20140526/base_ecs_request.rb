# Copyright 2015 ZhangZhaoyuan
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

require 'aliyunsdk/core/acs_request'
require 'aliyunsdk/core/utils/api_util'
module Aliyunsdk
  module Ecs
    class BaseECSRequest
      include Aliyunsdk::Core::AcsRequest

      API_VERSION = '2014-05-26'
      SIGNATURE_METHOD = 'HMAC-SHA1'.to_sym
      SIGNATURE_VERSION = '1.0'
      attr_reader :product_name

      def initialize(format)
        @product_name = :ECS
        @param_keys ||= []
        @param_keys.each { |param|
          BaseECSRequest.class_eval(%Q(attr_accessor :#{Aliyunsdk::Core::ApiUtil.underscore param})) }
        super(format, SIGNATURE_METHOD, SIGNATURE_VERSION, API_VERSION)
      end

      def generate_sign_string
        @param_keys.each do |key|
          @signature_params[key] = self.instance_variable_get("@#{Aliyunsdk::Core::ApiUtil.underscore key.to_s}")
        end
        @signature_params.delete_if { |k, v| v.nil? }
        super
      end
    end
  end
end