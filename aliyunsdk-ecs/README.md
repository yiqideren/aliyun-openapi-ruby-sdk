# Aliyunsdk::Ecs

Ruby SDK for aliyun ecs

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'aliyunsdk-ecs'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install aliyunsdk-ecs

## Usage