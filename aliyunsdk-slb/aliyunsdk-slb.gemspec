# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'aliyunsdk/slb/version'

Gem::Specification.new do |spec|
  spec.name = "aliyunsdk-slb"
  spec.version = Aliyunsdk::Slb::VERSION
  spec.authors = ["ZhangZhaoyuan"]
  spec.email = ["doraemon.zh@gmail.com"]
  spec.license = 'Apache-2.0'

  spec.summary = %q{Ruby SDK for aliyun SLB openapi}
  spec.homepage = "http://git.oschina.net/doraemon/aliyun-openapi-ruby-sdk"

  spec.files = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.10"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "minitest", "~> 5.4.3", ">= 5.4.3"

  spec.add_runtime_dependency "aliyunsdk-core", ">= 0.1.0"
end
