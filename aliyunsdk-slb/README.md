# Aliyunsdk::Slb

Ruby SDK for aliyun openapi. Support Server Load Balancer(SLB) product.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'aliyunsdk-slb'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install aliyunsdk-slb

## Usage

1. Set app key id and app key secret

```ruby
require 'aliyunsdk/core/client'
Aliyunsdk::Core::Client::AcsClient.access_key_id ='your access key id'
Aliyunsdk::Core::Client::AcsClient.access_key_secret ='your access key secret'
```

2. Create AcsClient

```ruby
client = Aliyunsdk::Core::Client::AcsClient.new
```

3. Create AcsRequest. Parameter is 'JSON' or 'XML'

```ruby
request = Aliyunsdk::Slb::CreateLoadBalancer.new 'XML'
```

4. Set OpenAPI parameters.

```ruby
request.region_id = 'cn-beijing'
request.load_balancer_name = 'for_test'
```

5. Send request.

```ruby
response = client.do_action request
```

6. Get response. `response` is a Hash object.

```ruby
puts response['RequestId']
```

## Contributing

Bug reports and pull requests are welcome on OscGit at http://git.oschina.net/doraemon/aliyun-openapi-ruby-sdk.

