require 'test_helper'

class Aliyunsdk::SlbTest < Minitest::Test
  def test_that_it_has_a_version_number
    refute_nil ::Aliyunsdk::Slb::VERSION
  end

  def test_send_request_success
    require 'aliyunsdk/slb/request/v20140515/describe_regions'
    request = Aliyunsdk::Slb::DescribeRegions.new 'XML'
    client = Aliyunsdk::Core::Client::AcsClient.new
    assert_includes client.do_action(request)["Regions"]["Region"],
                    {"RegionId" => "cn-beijing", "LocalName" => "北京"}, 'Send request failed!'
  end

  def test_add_backend_servers
    require 'aliyunsdk/slb/request/v20140515/add_backend_servers'
    request = Aliyunsdk::Slb::AddBackendServers.new 'XML'
    client = Aliyunsdk::Core::Client::AcsClient.new
    request.add_backend_server 'server_id', '100'
    request.add_backend_server 'server_id2', '10'
    request.load_balancer_id = '1503b67fbb0-cn-beijing-btc-a01'
    res = client.do_action request
    refute_nil res['RequestId'], 'test add backend servers failed!'
  end

  def test_remove_backend_servers
    require 'aliyunsdk/slb/request/v20140515/remove_backend_servers'
    request = Aliyunsdk::Slb::RemoveBackendServers.new 'XML'
    client = Aliyunsdk::Core::Client::AcsClient.new
    request.add_backend_server 'server_id'
    request.add_backend_server 'server_id2'
    request.load_balancer_id = '1503b67fbb0-cn-beijing-btc-a01'
    res = client.do_action request
    refute_nil res['RequestId'], 'test_remove_backend_servers failed!'
  end

  def test_create_load_balancer
    require 'aliyunsdk/slb/request/v20140515/create_load_balancer'
    request = Aliyunsdk::Slb::CreateLoadBalancer.new 'XML'
    client = Aliyunsdk::Core::Client::AcsClient.new
    request.region_id = 'cn-beijing'
    request.load_balancer_name = 'for_test'
    request.client_token = '123'
    res = client.do_action request
    refute_nil res['RequestId'], 'test_create_load_balancer failed!'
  end
end
