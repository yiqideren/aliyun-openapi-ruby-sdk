# Copyright 2015 ZhangZhaoyuan
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

require 'aliyunsdk/slb/request/v20140515/base_slb_request'
require 'json'
module Aliyunsdk
  module Slb
    class AddBackendServers < BaseSLBRequest

      def initialize(format)
        @signature_params = {Action: 'AddBackendServers'}
        @param_keys = %i(LoadBalancerId BackendServers)
        super
      end

      # Add Backend Server
      # @param server_id [String]
      # @param weight [String]
      def add_backend_server(server_id, weight)
        @backend_servers ||= []
        @backend_servers << {ServerId: server_id, Weight: weight}
      end

      def generate_sign_string
        @backend_servers = JSON.generate(@backend_servers) if @backend_servers.is_a? Array
        super
      end
    end
  end
end