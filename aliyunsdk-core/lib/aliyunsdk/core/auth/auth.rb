# Copyright 2015 ZhangZhaoyuan
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

require 'openssl'
require 'base64'
require 'uri'
require 'aliyunsdk/core/errors'
require 'aliyunsdk/core/client'

module Aliyunsdk
  module Core
    class Auth

      SIGNATURE_METHOD = {'HMAC-SHA1': 'sha1'}

      # Get Signature
      # @param sign_string [String] sign string
      # @param sig_method [Symbol] signature method
      # @return [String] signature
      def self.sign(sign_string, sig_method)
        secret = Client::AcsClient.access_key_secret
        begin
          digest = OpenSSL::Digest.new SIGNATURE_METHOD[sig_method]
          hmac = OpenSSL::HMAC.digest digest, "#{secret}&", sign_string
          result = Base64.encode64(hmac)
        rescue Exception
          raise AuthException, "Sign Error!Message is #{$!} at #{$@}"
        end
        result
      end
    end
  end
end
