# Copyright 2015 ZhangZhaoyuan
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

require 'aliyunsdk/core/utils/api_util'
require 'aliyunsdk/core/http/http_request'
require 'aliyunsdk/core/errors'
module Aliyunsdk
  module Core
    module Client
      # Base Acs Client
      # @author ZhangZhy

      class AcsClient

        # Constructor for AcsClient
        # @param region_id [String] region id
        # @param user_agent [String] user agent
        def initialize(region_id=nil, user_agent=nil)
          @region_id = region_id
          @user_agent = user_agent
        end

        class << self
          attr_accessor :access_key_id, :access_key_secret
        end

        # Send request to OpenAPI
        # @param acs_request (see Aliyunsdk::Core::AcsRequest)
        def do_action(acs_request, **headers)
          domain = ApiUtil.find_api_address acs_request.product_name
          raise ClientException, "Can't find domain by product name#{acs_request.product_name}" if domain.eql? 'unknown'

          http_req = HttpRequest.new acs_request, domain, headers
          ApiUtil.parse_response http_req.get_response.body, acs_request.format
        end
      end
    end
  end
end