# Aliyunsdk::Core

Ruby SDK Core.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'aliyunsdk-core'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install aliyunsdk-core

## Contributing

Bug reports and pull requests are welcome on OscGit at http://git.oschina.net/doraemon/aliyun-openapi-ruby-sdk.

