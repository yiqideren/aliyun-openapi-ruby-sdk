require 'test_helper'

class Aliyunsdk::CoreTest < Minitest::Test
  def test_it_has_a_version_number
    refute_nil ::Aliyunsdk::Core::VERSION
  end

  def test_it_does_get_slb_address
    assert Aliyunsdk::Core::ApiUtil.find_api_address(:SLB) == 'slb.aliyuncs.com', 'Get Request Address Failed!'
  end

  def test_parse_fail_xml
    result = Aliyunsdk::Core::ApiUtil.parse_from_xml '<?xml version="1.0" encoding="UTF-8"?>
    <Error>
        <RequestId>8906582E-6722-409A-A6C4-0E7863B733A5</RequestId>
        <HostId>slb.aliyuncs.com</HostId>
        <Code>UnsupportedOperation</Code>
        <Message>The specified action is not supported.</Message>
    </Error>'
    assert('UnsupportedOperation'.eql?(result['Code']), "Parse fail xml failed!")
  end

  def test_parse_fail_json
    result = Aliyunsdk::Core::ApiUtil.parse_from_json '{
    "RequestId":"7463B73D-35CC-4D19-A010-6B8D65D242EF",
    "HostId":"slb.aliyuncs.com",
    "Code":"UnsupportedOperation",
    "Message":"The specified action is not supported."}'
    assert('UnsupportedOperation'.eql?(result['Code']), "Parse fail json failed!")
  end

  def test_parse_success_from_xml
    result = Aliyunsdk::Core::ApiUtil.parse_from_xml %q(<?xml version='1.0' encoding='UTF-8'?><DescribeRegionsResponse><RequestId>43425290-D82A-46B7-B680-A3774CBA6C63</RequestId><Regions><Region><RegionId>ap-southeast-1</RegionId><LocalName>亚太（新加坡）</LocalName></Region><Region><RegionId>cn-qingdao</RegionId><LocalName>青岛</LocalName></Region><Region><RegionId>cn-beijing</RegionId><LocalName>北京</LocalName></Region><Region><RegionId>cn-shanghai</RegionId><LocalName>上海</LocalName></Region><Region><RegionId>cn-shenzhen</RegionId><LocalName>深圳</LocalName></Region><Region><RegionId>cn-hongkong</RegionId><LocalName>香港</LocalName></Region><Region><RegionId>us-west-1</RegionId><LocalName>美国硅谷</LocalName></Region><Region><RegionId>cn-hangzhou</RegionId><LocalName>杭州</LocalName></Region></Regions></DescribeRegionsResponse>)
    puts result
    assert_includes result['Regions']['Region'], {"RegionId" => "cn-beijing", "LocalName" => "北京"}, 'Parse success json failed!'
  end

  def test_parse_success_from_json
    result = Aliyunsdk::Core::ApiUtil.parse_from_json %q({"RequestId":"80697776-205A-4BFD-BAEF-435F77C1C319","Regions":{"Region":[{"RegionId":"ap-southeast-1","LocalName":"亚太（新加坡）"},{"RegionId":"cn-qingdao","LocalName":"青岛"},{"RegionId":"cn-beijing","LocalName":"北京"},{"RegionId":"cn-shanghai","LocalName":"上海"},{"RegionId":"cn-shenzhen","LocalName":"深圳"},{"RegionId":"cn-hongkong","LocalName":"香港"},{"RegionId":"us-west-1","LocalName":"美国硅谷"},{"RegionId":"cn-hangzhou","LocalName":"杭州"}]}})
    assert_includes result['Regions']['Region'], {"RegionId" => "cn-beijing", "LocalName" => "北京"}, 'Parse success json failed!'
  end

  def test_string_underscore
    assert_equal Aliyunsdk::Core::ApiUtil.underscore('LoadMap'), 'load_map', 'string underscore failed!'
  end
end
